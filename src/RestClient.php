<?php
namespace CodeJunkii\Util;

/**
 * A Simple generic rest client
 *
 * @method string delete(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 * @method string get(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 * @method string patch(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 * @method string post(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 * @method string put(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 * @method string head(string $endpoint, [array $data[, array $curlOptions[, $encType=self::ENCTYPE_URLENCODED]]])
 */
class RestClient
{
    const ENCTYPE_MULTIPART     = 'multipart/form-data';
    const ENCTYPE_URLENCODED    = 'application/x-www-form-urlencoded';
    const ENCTYPE_TEXTPLAIN     = 'text/plain';

    private $_curlOptions = array(
        'CURLOPT_HEADER'=>false,
        'CURLOPT_RETURNTRANSFER'=>true,
        'CURLOPT_AUTOREFERER'=>false,
        'CURLOPT_COOKIESESSION'=>true,
        'CURLOPT_FOLLOWLOCATION'=>true,
        'CURLOPT_FORBID_REUSE'=>true,
        'CURLOPT_SSL_VERIFYPEER'=>false,
        'CURLOPT_SSL_VERIFYHOST'=>false,
        'CURLOPT_TIMEOUT'=>10,
        'CURLOPT_CONNECTTIMEOUT'=>5
    );

    /**
     * Supported HTTP verbs
     * @var array
     */
    private $_supportedVerbs = array(
        'GET',
        'POST',
        'DELETE',
        'PUT',
        'PATCH',
        'HEAD'
    );

    /**
     * @var string A username
     */
    private $_user = null;
    /**
     * @var string A password
     */
    private $_passwd = null;

    /**
     * A key-value pair array which key should be a CURLOPT_* and value the desired
     * value.
     * @var array
     */
    private $_options = array();

    /**
     * A file path to hold the cookie data
     * @var string
     */
    public $cookieFile = null;

    /**
     * Enables/ debugging
     * @var boolean
     */
    public $debug = false;

    /**
     * key-value pair
     * @var array
     */
    public $httpHeaders = array();

    /**
     * The user agent
     * @var string
     */
    public $userAgent = 'CodeJunkii-RestClient';

    /**
     * The referrer to be send otherwise the CURLOPT_AUTOREFERER=true will be user
     * @var string
     */
    public $referer = null;

    /**
     * Makes a request at a rest application.
     *
     * @throws CodeJunkii\Util\Exceptions\RestClientException if there are curl errors Or if the response doesn't contains a HTTP code
     * @param  string $httpMethod  A valid http verb
     * @param  string $url         The request url
     * @param  array $data         An array with the data to be send
     * @param  array  $curlOptions A key-value pair array with any additional curl options
     * @param  string $encType     The data encType. Default is 'application/x-www-form-urlencoded'
     * @return RestClientResponse  The response of the request wrapped in a custom object
     */
    public function sendRequest($httpMethod, $url, $data=null, $curlOptions=array(), $encType=self::ENCTYPE_URLENCODED)
    {
        $ch = curl_init();
        $options = $this->getOptions($curlOptions);
        $this->setMethod($httpMethod, $options);
        $_payload = null;
        if (!empty($data) && $encType !== self::ENCTYPE_MULTIPART) {
            $options[CURLOPT_POSTFIELDS] = $_payload = http_build_query($data, '&');
        }
        $options[CURLOPT_URL] = $url;

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        if ($response === false) {
            throw new Exceptions\RestClientException(curl_error($ch), curl_errno($ch));
        }

        return new RestClientResponse($response, $ch, $this);
    }

    /**
     * Set the basic auth details to be user at the request
     *
     * @param string $user
     * @param string $pass
     * @return CodeJunkii\Util\RestClient the method for chaining method call
     */
    public function setAuth($user, $pass)
    {
        $this->_user = $user;
        $this->_pass = $pass;

        return $this;
    }

    /**
     * Set additional curl options
     *
     * @param array $opts A key-value pair array with any additional curl options
     * @return CodeJunkii\Util\RestClient the method for chaining method call
     */
    public function setOptions($opts)
    {
        $this->_options = $opts;
        return $this;
    }

    /**
     * Applies the selected method on the curl options.
     * This will override any other options.
     *
     * @param string $method A valid HTTP verb
     * @param array  $options An array passed by reference with the current curl options
     */
    protected function setMethod($method, &$options)
    {
        $method = strtoupper($method);
        switch ($method) {
            case 'GET':
                $options[CURLOPT_HTTPGET]=true;
                break;
            case 'POST':
                $options[CURLOPT_POST]=true;
                break;
            case 'HEAD':
                $options[CURLOPT_NOBODY]=true;
                break;
            case 'PUT':
            case 'PATCH':
            case 'DELETE':
            default:
                $options[CURLOPT_CUSTOMREQUEST]=$method;
            break;
        }
    }

    /**
     * Returns an array with the options that will be used
     * at the request. The format will be the same as the
     * curl_setopt_array expects
     *
     * @param  array  $extraOpts A key-value pair array with additional options-+
     * @return array             A key-value pair array ready to used by the curl_setopt_array()
     */
    protected function getOptions($extraOpts=array())
    {
        $_opts = array_merge($this->_curlOptions, $this->_options, $extraOpts);

        $_o = array();
        foreach ($_opts as $key => $value) {
            if (is_string($key)) {
                $_o[constant('CURLOPT_' . str_replace('CURLOPT_', '', $key))] = $value;
            } else {
                $_o[$key] = $value;
            }
        }

        if (!empty($this->httpHeaders)) {
            $_o[CURLOPT_HTTPHEADER] = $this->httpHeaders;
        }

        if ($this->_user && $this->_pass) {
            $_o[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
            $_o[CURLOPT_USERPWD] = "{$this->_user}:{$this->_pass}";
        }

        if ($this->cookieFile !== null) {
            $_o[CURLOPT_COOKIEFILE] = $this->cookieFile;
            // SAME CURLOPT_COOKIEFILE
            $_o[CURLOPT_COOKIEJAR]  = $this->cookieFile;
        }

        if ($this->referer !== null) {
            $_o[CURLOPT_REFERER] = $this->referer;
        }

        if ($this->debug) {
            $_o[CURLINFO_HEADER_OUT] = true;
        }

        $_o[CURLOPT_USERAGENT] = $this->userAgent;

        return $_o;
    }

    /**
     * Magic method that overloads the sendRequest method
     * with the supported HTTP verbs.
     * Means that the first arguments on the sendRequest becomes
     * the method name.
     *
     * @param  string $method A valid HTTP verb.
     * @param  array  $params Any method params
     * @return CodeJunkii\Util\RestClientResponse
     */
    public function __call($method, $params)
    {
        if (in_array(strtoupper($method), $this->_supportedVerbs)) {
            array_unshift($params, $method);
            return call_user_func_array(array($this, 'sendRequest'), $params);
        }

        throw new \BadMethodCallException("Undefined method {$method}");
    }
}