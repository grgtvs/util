<?php
namespace CodeJunkii\Util;
/**
 * TODO drop a few lines here
 */
class RestClientResponse
{
    public $contentType = null;
    public $debugInfo = null;
    public $response;
    public $statusCode;
    public $status;

    /**
     * @throws CodeJunkii\Util\Exceptions\RestClientException If no HTTP code was returned
     * @param string $rawResponse
     * @param resource $curlResource [description]
     * @param CodeJunkii\Util\RestClient $restClient   [description]
     */
    public function __construct($rawResponse, $curlResource, $restClient)
    {
        $this->statusCode = curl_getinfo($curlResource, CURLINFO_HTTP_CODE);
        if (!$this->statusCode) {
            throw new Exceptions\RestClientException("Weird! No HTTP code was returned.");
        }

        $httpCodes = self::httpCodes();
        $this->status = $httpCodes[$this->statusCode];
        $this->contentType = curl_getinfo($curlResource, CURLINFO_CONTENT_TYPE);

        if ($restClient->debug) {
            $this->debugInfo = curl_getinfo($curlResource);
        }

        $this->response = $rawResponse;
    }

    public static function httpCodes()
    {
        return array(
            // [Informational 1xx]
            100=>'Continue',
            101=>'Switching Protocols',

            // [Successful 2xx]
            200=>'OK',
            201=>'Created',
            202=>'Accepted',
            203=>'Non-Authoritative Information',
            204=>'No Content',
            205=>'Reset Content',
            206=>'Partial Content',

            // [Redirection 3xx]
            300=>'Multiple Choices',
            301=>'Moved Permanently',
            302=>'Found',
            303=>'See Other',
            304=>'Not Modified',
            305=>'Use Proxy',
            306=>'(Unused)',
            307=>'Temporary Redirect',

            // [Client Error 4xx]
            400=>'Bad Request',
            401=>'Unauthorized',
            402=>'Payment Required',
            403=>'Forbidden',
            404=>'Not Found',
            405=>'Method Not Allowed',
            406=>'Not Acceptable',
            407=>'Proxy Authentication Required',
            408=>'Request Timeout',
            409=>'Conflict',
            410=>'Gone',
            411=>'Length Required',
            412=>'Precondition Failed',
            413=>'Request Entity Too Large',
            414=>'Request-URI Too Long',
            415=>'Unsupported Media Type',
            416=>'Requested Range Not Satisfiable',
            417=>'Expectation Failed',

            //[Server Error 5xx]
            500=>'Internal Server Error',
            501=>'Not Implemented',
            502=>'Bad Gateway',
            503=>'Service Unavailable',
            504=>'Gateway Timeout',
            505=>'HTTP Version Not Supported',
        );
    }
}
